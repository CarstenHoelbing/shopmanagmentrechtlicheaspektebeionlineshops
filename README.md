#Beschreibung

Diese Hausarbeit und deren Presentation wurden für des Fach "Shop-Managment" (Studiengang E-Commerce) bei Prof. Werner im Fachbereich Wirtschaftswissenschaften  an der [EAH-Jena](http://www.eah-jena.de) erstellt.
Aufgabe: Erstellen Sie eine Hausarbeit und eine Presentaion mit dem Titel "Rechtliche Aspekte bei Online-Shops".

## Beschreibung - Hausaufgabe
Die Hausaufgabe mit dem Titel "Rechtliche Aspekte bei Online-Shops" wurde erstellt von [Carsten Hölbing](www.hoelbing.net) im WS 2014/2015.
Lizenz: [CC-BY-SA-4.0](http://creativecommons.org/licenses/by-sa/4.0/) (Creative Commons Attribution-ShareAlike 4.0 International License)

## Beschreibung - Presentation
Die Presentation mit dem Titel "Rechtliche Aspekte bei Online-Shops" wurde erstellt von [Carsten Hölbing](www.hoelbing.net) im WS 2014/2015.
Lizenz: [CC-BY-SA-4.0](http://creativecommons.org/licenses/by-sa/4.0/) (Creative Commons Attribution-ShareAlike 4.0 International License)

## Software - LaTeX (LaTeX Project Public License)
Lizenz: [LPPL-1.3c](http://opensource.org/licenses/lppl) (LaTeX Project Public License)
The LaTeX Project Public License (LPPL) is the primary license under which the LaTeX kernel and the base LaTeX packages are distributed.
You may use this license for any work of which you hold the copyright and which you wish to distribute. This license may be particularly suitable if your work is TeX-related (such as a LaTeX package), but it is written in such a way that you can use it even if your work is unrelated to TeX.
The section `WHETHER AND HOW TO DISTRIBUTE WORKS UNDER THIS LICENSE', below, gives instructions, examples, and recommendations for authors who are considering distributing their works under this license.
This license gives conditions under which a work may be distributed and modified, as well as conditions under which modified versions of that work may be distributed.
We, the LaTeX3 Project, believe that the conditions below give you the freedom to make and distribute modified versions of your work that conform with whatever technical specifications you wish while maintaining the availability, integrity, and reliability of that work. If you do not see how to achieve your goal while meeting these conditions, then read the document `cfgguide.tex' and `modguide.tex' in the base LaTeX distribution for suggestions. 

## Software - reveal.js
Lizenz:  [CC-BY-SA-4.0](http://creativecommons.org/licenses/by-sa/4.0/) (Creative Commons Attribution-ShareAlike 4.0 International License)
You will find a presentation under `presentation`. It is made using [reveal.js](https://github.com/hakimel/reveal.js). It is licensed under CC-BY-SA-4.0. So feel free to use it but please do not forget to attribtue it and share under the same license. 
